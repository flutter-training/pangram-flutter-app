import 'package:random_pangram/app.dart' as app;
import 'package:flutter/widgets.dart';
import 'package:random_pangram/common/config/injector.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupInjections();
  app.main();
}

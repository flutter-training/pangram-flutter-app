import 'package:flutter/material.dart';

class PangramEntity {
  final String title;
  final String author;
  final String pangram;

  PangramEntity({
    @required this.title,
    @required this.author,
    @required this.pangram,
  });
}

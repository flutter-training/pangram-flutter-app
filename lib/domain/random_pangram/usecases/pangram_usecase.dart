import 'package:injectable/injectable.dart';
import 'package:random_pangram/common/models/usecase.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';
import 'package:random_pangram/domain/random_pangram/repositories/pangram_repository.dart';

@injectable
@lazySingleton
class GetRandomPangramUsecase implements UseCase<PangramEntity, NoPayload> {
  final PangramRepository repo;

  GetRandomPangramUsecase(this.repo);
  @override
  Future<PangramEntity> call(NoPayload _) {
    return repo.getRandomPangram();
  }
}

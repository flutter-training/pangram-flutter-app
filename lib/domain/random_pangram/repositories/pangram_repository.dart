import 'package:injectable/injectable.dart';
import 'package:random_pangram/data/random_pangram/repositories/pangram_repository_impl.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';

@Bind.toType(PangramRepositoryImpl)
@injectable
abstract class PangramRepository {
  Future<PangramEntity> getRandomPangram();
}

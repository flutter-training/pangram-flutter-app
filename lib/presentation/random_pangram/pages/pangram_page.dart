import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:random_pangram/common/config/injector.dart';
import 'package:random_pangram/presentation/random_pangram/bloc/pangram_bloc.dart';
import 'package:random_pangram/presentation/random_pangram/pages/widgets/pangram_card.dart';
import 'package:random_pangram/presentation/random_pangram/pages/widgets/refresh_pangram_button.dart';

class PangramPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pangram of The Day'),
      ),
      body: SafeArea(
        child: Center(
          child: BlocProvider<PangramBloc>(
            create: (context) =>
                getIt<PangramBloc>()..add(PangramLoadingEvent()),
            child: BlocConsumer<PangramBloc, PangramState>(
              builder: (context, state) {
                if (state is PangramLoaded) {
                  return PangramCard(pangram: state.pangram);
                } else if (state is PangramInitial) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Initial'),
                      RefreshPangramButton(),
                    ],
                  );
                } else {
                  return CircularProgressIndicator();
                }
              },
              listener: (context, state) {},
            ),
          ),
        ),
      ),
    );
  }
}

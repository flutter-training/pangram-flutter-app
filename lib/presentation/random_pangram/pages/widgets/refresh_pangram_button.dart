import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:random_pangram/presentation/random_pangram/bloc/pangram_bloc.dart';

class RefreshPangramButton extends StatelessWidget {
  const RefreshPangramButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      icon: Icon(Icons.replay),
      label: Text('Get Another Pangram'),
      onPressed: () {
        BlocProvider.of<PangramBloc>(context).add(PangramLoadingEvent());
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';
import 'package:random_pangram/presentation/random_pangram/pages/widgets/refresh_pangram_button.dart';

class PangramCard extends StatelessWidget {
  const PangramCard({Key key, @required this.pangram}) : super(key: key);

  final PangramEntity pangram;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: 300,
          child: ListTile(
            title: Text(pangram.pangram),
            subtitle: Text('${pangram.title} - ${pangram.author}'),
          ),
        ),
        RefreshPangramButton(),
      ],
    );
  }
}

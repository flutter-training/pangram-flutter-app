part of 'pangram_bloc.dart';

@immutable
abstract class PangramEvent {}

@immutable
class PangramLoadingEvent extends PangramEvent {}

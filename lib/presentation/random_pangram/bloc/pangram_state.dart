part of 'pangram_bloc.dart';

@immutable
abstract class PangramState {}

@immutable
class PangramInitial extends PangramState {}

@immutable
class PangramLoading extends PangramState {}

@immutable
class PangramLoaded extends PangramState {
  final PangramEntity pangram;

  PangramLoaded(this.pangram);
}

@immutable
class PangramError extends PangramState {
  final String message;
  final dynamic error;

  PangramError({@required this.message, this.error});
}

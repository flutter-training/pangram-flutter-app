import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:random_pangram/common/config/injector.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';
import 'package:random_pangram/domain/random_pangram/usecases/pangram_usecase.dart';

part 'pangram_event.dart';
part 'pangram_state.dart';

@injectable
@lazySingleton
class PangramBloc extends Bloc<PangramEvent, PangramState> {
  @override
  PangramState get initialState => PangramInitial();

  final GetRandomPangramUsecase getRandom = getIt<GetRandomPangramUsecase>();

  @override
  Stream<PangramState> mapEventToState(
    PangramEvent event,
  ) async* {
    if (event is PangramLoadingEvent) {
      yield PangramLoading();
      PangramEntity pangram = await getRandom(null);
      yield PangramLoaded(pangram);
    }
  }
}

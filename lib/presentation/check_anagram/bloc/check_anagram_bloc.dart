import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'check_anagram_event.dart';
part 'check_anagram_state.dart';

class CheckAnagramBloc extends Bloc<CheckAnagramEvent, CheckAnagramState> {
  @override
  CheckAnagramState get initialState => CheckAnagramInitial();

  @override
  Stream<CheckAnagramState> mapEventToState(
    CheckAnagramEvent event,
  ) async* {
    // TODO: Add Logic
  }
}

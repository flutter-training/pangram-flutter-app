part of 'check_anagram_bloc.dart';

@immutable
abstract class CheckAnagramState {}

@immutable
class CheckAnagramInitial extends CheckAnagramState {}

@immutable
class CheckAnagramChecking extends CheckAnagramState {}

@immutable
class CheckAnagramChecked extends CheckAnagramState {}

@immutable
class CheckAnagramError extends CheckAnagramState {}

import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:random_pangram/common/network/pangram_client.dart';
import 'package:random_pangram/data/random_pangram/datasources/pangram_datasource.dart';
import 'package:random_pangram/data/random_pangram/models/pangram_model.dart';
import 'package:http/http.dart' as http;

@lazySingleton
@injectable
class PangramRemoteDatasource implements PangramDatasource {
  final PangramClient _client;

  PangramRemoteDatasource(this._client);

  @override
  Future<PangramModel> getRandomPangram() async {
    Uri uri = Uri.https('random-panagram.herokuapp.com', '/pangram');
    http.Response res = await _client.get(uri);
    PangramModel pangram = PangramModel.fromJson(jsonDecode(res.body));
    return pangram;
  }
}

import 'package:injectable/injectable.dart';
import 'package:random_pangram/data/random_pangram/datasources/pangram_remote_datasource.dart';
import 'package:random_pangram/data/random_pangram/models/pangram_model.dart';

@Bind.toType(PangramRemoteDatasource)
@injectable
abstract class PangramDatasource {
  Future<PangramModel> getRandomPangram();
}

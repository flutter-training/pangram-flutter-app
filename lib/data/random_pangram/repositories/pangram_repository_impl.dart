import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:random_pangram/data/random_pangram/datasources/pangram_datasource.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';
import 'package:random_pangram/domain/random_pangram/repositories/pangram_repository.dart';

@lazySingleton
@injectable
class PangramRepositoryImpl implements PangramRepository {
  final PangramDatasource datasource;

  PangramRepositoryImpl({@required this.datasource});

  @override
  Future<PangramEntity> getRandomPangram() {
    return datasource.getRandomPangram();
  }
}

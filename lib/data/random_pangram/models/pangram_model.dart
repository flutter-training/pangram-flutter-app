import 'package:flutter/material.dart';
import 'package:random_pangram/domain/random_pangram/entities/pangram_entity.dart';

class PangramModel extends PangramEntity {
  PangramModel({
    @required String title,
    @required String author,
    @required String pangram,
  }) : super(
          title: title,
          author: author,
          pangram: pangram,
        );

  factory PangramModel.fromJson(Map<String, dynamic> json) {
    return PangramModel(
      title: json['title'],
      author: json['author'],
      pangram: json['pangram'],
    );
  }
}

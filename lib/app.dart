import 'package:flutter/material.dart';
import 'package:random_pangram/common/routes/router.dart';
import 'package:random_pangram/common/routes/routes.dart';
import 'package:random_pangram/common/widgets/drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: Router.onGeneratedRoute,
      onUnknownRoute: Router.onUnknownRoute,
      // initialRoute: Routes.pangram,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Judul'),
        ),
        drawer: CustomDrawer(),
        body: Text('data'));
  }
}

import 'package:flutter/material.dart';
import 'package:random_pangram/common/routes/routes.dart';
import 'package:random_pangram/presentation/check_anagram/pages/check_anagram_page.dart';
import 'package:random_pangram/presentation/random_pangram/pages/pangram_page.dart';

abstract class Router {
  static Route<dynamic> onGeneratedRoute(RouteSettings settings) {
    final _args = settings.arguments;
    switch (settings.name) {
      case Routes.pangram:
        return MaterialPageRoute(builder: (_) => PangramPage());
      case Routes.anagram:
        return MaterialPageRoute(builder: (_) => CheckAnagramPage());
      default:
        return onGeneratedRoute(settings);
    }
  }

  static Route<dynamic> onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) => Scaffold(
        body: Center(
          child: Text('Error'),
        ),
      ),
    );
  }
}

abstract class Routes {
  static const String home = '/';
  static const String pangramPath = 'panagram';
  static const String pangram = '$home/$pangramPath';
  static const String anagramPath = 'anagram';
  static const String anagram = '$home/$anagramPath';
}

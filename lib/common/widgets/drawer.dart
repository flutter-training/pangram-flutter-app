import 'package:flutter/material.dart';
import 'package:random_pangram/common/routes/routes.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            AppBar(
              title: Text('App Title'),
              automaticallyImplyLeading: false,
            ),
            FlatButton(
              child: Text('Pangram'),
              onPressed: () => Navigator.of(context).pushNamed(Routes.pangram),
            ),
            FlatButton(
              child: Text('Check Anagram'),
              onPressed: () => Navigator.of(context).pushNamed(Routes.anagram),
            ),
          ],
        ),
      ),
    );
  }
}

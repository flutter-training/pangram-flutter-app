// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:random_pangram/common/network/pangram_client.dart';
import 'package:http/src/client.dart';
import 'package:random_pangram/data/random_pangram/datasources/pangram_datasource.dart';
import 'package:random_pangram/data/random_pangram/datasources/pangram_remote_datasource.dart';
import 'package:random_pangram/domain/random_pangram/repositories/pangram_repository.dart';
import 'package:random_pangram/data/random_pangram/repositories/pangram_repository_impl.dart';
import 'package:random_pangram/domain/random_pangram/usecases/pangram_usecase.dart';
import 'package:random_pangram/presentation/random_pangram/bloc/pangram_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<PangramClient>(() => PangramClientImpl(getIt<Client>()))
    ..registerLazySingleton<PangramClientImpl>(
        () => PangramClientImpl(getIt<Client>()))
    ..registerFactory<PangramDatasource>(
        () => PangramRemoteDatasource(getIt<PangramClient>()))
    ..registerLazySingleton<PangramRemoteDatasource>(
        () => PangramRemoteDatasource(getIt<PangramClient>()))
    ..registerFactory<PangramRepository>(
        () => PangramRepositoryImpl(datasource: getIt<PangramDatasource>()))
    ..registerLazySingleton<PangramRepositoryImpl>(
        () => PangramRepositoryImpl(datasource: getIt<PangramDatasource>()))
    ..registerLazySingleton<GetRandomPangramUsecase>(
        () => GetRandomPangramUsecase(getIt<PangramRepository>()))
    ..registerLazySingleton<PangramBloc>(() => PangramBloc());
}

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:random_pangram/common/network/pangram_client.dart';

void main() {
  test('description', () async {
    Uri uri = Uri.http("random-panagram.herokuapp.com", "/pangram");
    PangramClient client = PangramClientImpl(http.Client());

    http.Response response = await client.get(uri);

    assert(response.body.isNotEmpty);
  });
}
